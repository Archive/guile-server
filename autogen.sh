#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="Gnome Guile Server"

(test -f $srcdir/configure.in \
  && test -f $srcdir/Guile.idl \
  && test -f $srcdir/gnome-guile-repl.c) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level Guile Server directory"
    exit 1
}

. $srcdir/macros/autogen.sh

